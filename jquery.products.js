/*
 * B2W
 * 
 *
 * Copyright (c) 2013 trsouz
 * Licensed under the MIT license.
 */

(function ($) {

  var global_container = {};
  var default_structure = {
    id: null,
    title: '',
    title_short: '', //title max 60
    sku: [],
    url: '',
    description: '',
    technical: {
      brand: '',
      model: '',
      info: ''
    },
    items_included: [],
    price: {
      from: '',
      sale: '',
      invoice: '',
      discount: ''
    },
    department: {
      id: null,
      title: '',
      url: ''
    },
    line: {
      id: null,
      title: '',
      url: ''
    },
    subline: {
      id: null,
      title: '',
      url: ''
    },
    image: '', // images.cover.normal
    images: {
      cover: {
        square: '', // 80x80
        small: '', // 150x150
        normal: '', // 250x250
        large: '' // 500x500
      },
      gallery: [] // Array
    },
    installments: {},
    card: {},
    rating: {
      overall: '',
      percent: '',
      review: '',
      image: ''
    },
    freight: ''
  };

  // Product Class
  function Product(values) {
    this.attributes = $.extend(true, {}, default_structure, values);
    $.extend(true, this, {
      update: function(args){
        $.extend(true, this.attributes, args);
      },
      get: function(attr){
        return this.attributes[attr];
      }
    });
  }

  $.extend(true, Product.prototype, default_structure);

  var counter = 1;
  function Container(args) {
    var self = this;
    self.callback_param = 'jqueryProductsCallback';
    return $.extend(this, {
        url: (function () {
          var host = window.location.host,
            host_rt;
          if (host.indexOf('americanas.com.br') != -1) host_rt = 'americanas';
          if (host.indexOf('submarino.com.br') != -1) host_rt = 'submarino';
          if (host.indexOf('shoptime.com.br') != -1) host_rt = 'shoptime';
          if (host.indexOf('soubarato.com.br') != -1) host_rt = 'soubarato';
          if (host_rt) return 'http://www.' + host_rt + '.com.br/productinfo?callback=?';
          return '/productinfo?callback=?';
        }).call(self),
        fetch: function () {
          $.ajax({
            url: self.url,
            data: {
              itens: self.pluck('id').sort().join(',')
            },
            jsonpCallback: self.callback_param,
            cache: true,
            dataType: 'jsonp',
            error: function () {
              self.error.apply(self, arguments);
            },
            success: function () {
              self.success.apply(self, arguments);
            },
            complete: function () {
              self.complete.apply(self, arguments);
            }
          });
          return this;
        }
      }
    );
  }
  $.extend(true, Container.prototype, $.fn, {
    success: function (data) {
      this.add(this.parse(data));
      this.trigger('success');
    },
    error: function () {
      this.trigger('error', arguments);
    },
    complete: function () {
      this.trigger('complete', arguments);
    },
    has: function(id){
      return this.get(id) !== null;
    },
    at: function(index){
      return $.fn.get.call(this, index);
    },
    get: function(id){
      for(var i = 0; i < this.length; i++){
        if(this[i].get('id') == id) return this[i];
      }
      return null;
    },
    add: function (info) {
      var self = this;
      if($.isArray(info)){
        $.each(info, function (a, b) {
          self.add(b);
        });
        return this;
      }
      if(typeof info == 'string' || typeof info == 'number'){
        return self.add({id: info + ''});
      }

      if(typeof info == 'object'){
        var prod = null;
        if(info.hasOwnProperty('id')){
          prod = self.get(info.id);
          if(prod){
            prod.update(info);
            self.trigger('update', prod);
          }
        }
        if(!prod){
          prod = new Product(info);
          self.push(prod);
          self.trigger('add', prod);
        }
      }
      return this;
    },
    pluck: function (attr) {
      var self = this;
      var rt = [];
      self.each(function (index, value) {
        if(value.get(attr))
          rt.push(value.get(attr));
      });
      return rt;
    },
    parse: function (response) {
      return mapFromJSON(response.products);
    }
  });

// Interface JSON like: www.submarino.com.br/productinfo?itens=111979303
  function mapFromJSON(values) {
    if($.isArray(values)){
      var list = [];
      $.each(values, function(i,v){
        list.push(mapFromJSON(v));
      });
      return list;
    }
    var obj = $.extend(true, {}, default_structure);
    for (var i in values) {
      var value = values[i];
      switch (i) {
        case 'name':
          obj.title = value;
          break;

        case 'croppedName':
          obj.title_short = value;
          break;

        case 'installment':
          obj.installments[value.total_installments] = value.installment_value;
          break;

        // Image Map
        case 'image':
          obj.images.cover.small = value;
          break;

        case 'mqImage':
          obj.image = value;
          obj.images.cover.normal = value;
          break;

        // Price Map
        case 'default_price':
          obj.price.from = value;
          break;

        case 'sales_price':
          obj.price.sale = value;
          break;

        case 'invoice_price':
          obj.price.invoice = value;
          break;

        case 'descmkt':
          obj.price.discount = value;
          break;

        // Rating
        case 'averageOverallRating':
          obj.rating.overall = value;
          break;

        case 'averageRatingPercent':
          obj.rating.percent = value;
          break;

        case 'numReviews':
          obj.rating.review = value;
          break;

        case 'ratingsImage':
          obj.rating.image = value;
          break;

        case 'freight':
          var img = value.toLowerCase();
          var final = '';
          img = img.split('/');
          img = img[img.length - 1];
          img = img.split('.')[0];

          switch(img){
            case "frete_capitais_rj_sp":
              final = "frete grátis capitais RJ e SP"
               break;

            case "fretegratis34":
            case "frete_frete_gratis":
            case "frete_gratis":
            case "frete_sul_sudeste_centro_nordeste":
            case "frete_sul_sudeste_nordeste":
              final = "frete grátis"
               break;

            case "frete_rj_sp":
            case "frete_gratis_rj_sp":
              final = "frete grátis RJ e SP"
               break;

            case "frete_sp":
            case "frete_gratis_sp":
              final = "frete grátis SP"
               break;

            case "frete_nordeste":
              final = "frete grátis nordeste"
               break;

            case "frete_sudeste":
              final = "frete grátis sudeste"
               break;

            case "frete_sul":
              final = "frete grátis sul"
               break;

            case "frete_sul_sudeste":
            case "frete_sul_sudeste_nordeste":
              final = "frete grátis sul e sudeste"
               break;

            case "frete_sul_sudeste_centro":
            case "frete_sul_sudeste_centro-1":
              final = "frete grátis sul, sudeste e CO"
               break;

            default:
              final = "";
          }
          obj.freight_name = final;
          obj[i] = value;
          break;

        default:
          if (typeof obj[i] != 'undefined')
            obj[i] = value;
      }
    }
    return obj;
  }

// PUBLIC

  $.products = function () {
    return new Container(arguments);
  };
  $.product = function () {
    return new Product(arguments);
  };
})(jQuery);
